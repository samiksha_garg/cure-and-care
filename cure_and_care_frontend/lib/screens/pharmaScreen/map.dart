import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cure_and_care_frontend/models/pharmacy_model.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter/services.dart';
import 'package:cure_and_care_frontend/components/loadingBar.dart';

class CustomMap extends StatefulWidget {
  PharmacyModel pharmacy;
  final bool loading;
  final LatLng initialPosition;
  final Function(CameraPosition) onCameraMove;
  CustomMap({
    required this.pharmacy,
    required this.loading,
    required this.initialPosition,
    required this.onCameraMove
  });
  _CustomMapState createState() => _CustomMapState();
}

class _CustomMapState extends State<CustomMap> {
// late PolylinePoints polylinePoints;
// List<LatLng> polylineCoordinates = [];
// Set<Polyline> _polylines = Set<Polyline>();
// late String mapsApiKey;
late GoogleMapController controller;

// Future<void> getMapsKey() async {
//     String mapsApiKeyfile = await rootBundle.loadString("secrets/maps_api_key.json");
//     var jsonAddress = jsonDecode(mapsApiKeyfile);
//     setState(() {
//       mapsApiKey = jsonAddress["maps_api_key"];
//     });
//     print(mapsApiKey);
// } 

// @override
// void initState() {
//   super.initState();
//   polylinePoints = PolylinePoints();
//   getMapsKey();
// }

@override
  void dispose() {
    // controller.dispose();
    super.dispose();
}

Set<Marker> _createMarker() {
    print(widget.pharmacy);
    Set<Marker> markers= Set();
    markers.add(Marker(
        markerId: MarkerId("marker"),
        position: LatLng(widget.pharmacy.pharmacyLatitude, widget.pharmacy.pharmacyLongitude),
        infoWindow: InfoWindow(title: "marker"),
      ));
      return markers;
  }

onMapCreated(GoogleMapController controller) {
    setState(() {
      controller = controller;
    });
    // setPolylines();
  }

  MapType _currentMapType = MapType.normal;

  // void setPolylines() async{
  //   PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
  //     mapsApiKey, 
  //     PointLatLng(widget.initialPosition.latitude, widget.initialPosition.longitude), 
  //     PointLatLng(widget.pharmacy.pharmacyLatitude, widget.pharmacy.pharmacyLongitude));
  //     print("NO");
  //       if(result.status == 'OK'){
  //         print("Yes");
  //         result.points.forEach((PointLatLng point){
  //           polylineCoordinates.add(LatLng(point.latitude, point.longitude));
  //         });
  //       }
  //       setState((){
  //         _polylines.add(
  //           Polyline(
  //             width: 100,
  //             polylineId: PolylineId('polyline'),
  //             color: Colors.red,
  //             points: polylineCoordinates
  //           )
  //         );
  //       });
  // }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  

  @override
  Widget build(BuildContext context) {
    if (widget.loading)
      return LoadingBarWidget(
              isLoading: true,
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.white,
              ),
      );

    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: widget.initialPosition,
        zoom: 11.0,
        tilt: 0,
        bearing: 0,
      ),
      onMapCreated: onMapCreated,
      zoomGesturesEnabled: true,
      onCameraMove: widget.onCameraMove,
      myLocationEnabled: true,
      // polylines: _polylines,
      compassEnabled: true,
      myLocationButtonEnabled: false,
      markers: _createMarker(),
    );
  }
}
