import 'package:flutter/material.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:cure_and_care_frontend/models/pharmacy_model.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cure_and_care_frontend/screens/PharmaScreen/pharmacy_type.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'dart:math';
import 'package:cure_and_care_frontend/utils/colors.dart';

class PharmacyTile extends StatefulWidget {
  final PharmacyModel pharmacy;
  final double distance;
  final bool loading;
  final LatLng initialPosition;
  final Function(CameraPosition) onCameraMove;
  const PharmacyTile({
    super.key,
    required this.pharmacy,
    required this.distance,
    required this.loading,
    required this.initialPosition,
    required this.onCameraMove,
  });
  @override
  State<PharmacyTile> createState() => _PharmacyTile();
}

class _PharmacyTile extends State<PharmacyTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.pharmacy.pharmacyName,
                    style: TextStyleConstants.s16w500black,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Distance: ' + widget.distance.toStringAsFixed(3) + 'km',
                    style: TextStyleConstants.s15wblack,
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CustomMap(
                    pharmacy: widget.pharmacy, 
                    loading: widget.loading, 
                    initialPosition: widget.initialPosition,
                    onCameraMove: widget.onCameraMove,
                    ),
                  ),
                );
                },
                child: Icon(
                  Icons.location_on_rounded,
                  color: HexColor(HexColors.seaPink),
                  size: 30.0,
                ),
              ),
            ],
          ),
          Divider(
              color: HexColor(HexColors.black)
          ),
        ],
      ),
    );
  }
}
