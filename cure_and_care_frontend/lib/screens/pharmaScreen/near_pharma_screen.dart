import 'dart:async';
import 'dart:math';

import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/pharmacy_type.dart';
import 'package:flutter/material.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:cure_and_care_frontend/models/pharmacy_model.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/map.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/pharmacy_tile.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/pharmacy_type.dart';
import 'package:haversine_distance/haversine_distance.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:cure_and_care_frontend/components/loadingBar.dart';

class NearPharmaScreen extends StatefulWidget {
  const NearPharmaScreen({super.key});

  @override
  State<NearPharmaScreen> createState() => _NearPharmaScreenState();
}

class _NearPharmaScreenState extends State<NearPharmaScreen> {
  static late LatLng initialPosition;
  static late LatLng _lastMapPosition;
  bool loading = true;
  final haversineDistance = HaversineDistance();
 

  Future _getUserLocation() async {
    LocationPermission permission;
    permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    print(position);
    setState(() {
      initialPosition = LatLng(position.latitude, position.longitude);
      _lastMapPosition = initialPosition;
    });
    setState(() {
      loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _getUserLocation();
  }

  double _getDistance(double lat, double long) {
    // Eucledian Distance
    // return sqrt(pow(_lastMapPosition.longitude - long, 2) + pow(_lastMapPosition.latitude - lat, 2));
    // Haversine_Distance
    return haversineDistance.haversine(new Location(_lastMapPosition.latitude, _lastMapPosition.longitude), new Location(lat, long), Unit.KM);
  }

  onCameraMove(CameraPosition position) {
    initialPosition= position.target;
  }

  @override
  Widget build(BuildContext context) {
    if (loading)
      return LoadingBarWidget(
              isLoading: true,
                child: Container(),
      );
    return Scaffold( 
      appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "Pharmacies",
              style: TextStyleConstants.s22w600black,
            ),
          ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('pharmacies').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: LoadingBarWidget(
              isLoading: true,
                child: Container(),
              ),
            );
          }
          else{
            var snapData = snapshot.data!.docs.toList();
            snapData.sort((pharA, pharB) => _getDistance(
              pharA['pharmacyLatitude'], pharA['pharmacyLongitude'])
          .compareTo(_getDistance(pharB['pharmacyLatitude'], pharB['pharmacyLongitude'])));
            return Container(
                child: ListView(
                  children: snapData.map((document) {
                  
                  return Container(
                  child: PharmacyTile(
                    pharmacy: PharmacyModel(
                      pharmacyLatitude:document['pharmacyLatitude'], 
                      pharmacyLongitude: document['pharmacyLongitude'], 
                      pharmacyName:document['pharmacyName'], 
                      pharmacyOwnerId: document['pharmacyOwnerId'], 
                      pharmacyDrugs: document['pharmacyDrugs'],
                      ),
                    distance: _getDistance(document['pharmacyLatitude'], document['pharmacyLongitude']),
                    loading: loading, 
                    initialPosition: _lastMapPosition, 
                    onCameraMove: onCameraMove
                    ),
              );
            }).toList(),
          ),
        );
        }
        },
      ), 
    );  
  }
}