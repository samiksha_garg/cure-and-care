import 'package:cure_and_care_frontend/screens/authScreens/register_doctor.dart';
import 'package:cure_and_care_frontend/screens/authScreens/register_patient.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RegisterationScreen extends StatelessWidget {
  const RegisterationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 15.h),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                  child: Text(
                "Welcome!",
                style: TextStyleConstants.s22w700black,
              )),
              Center(
                  child: Text(
                "Register Yourself As",
                style: TextStyleConstants.s22w700black,
              )),
              SizedBox(
                height: 30.h,
              ),
              RegisterButton(
                title: "Doctor",
                color: HexColor(HexColors.denimBlue),
                function: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const RegisterDoctorScreen()));
                },
              ),
              SizedBox(
                height: 15.h,
              ),
              RegisterButton(
                title: "Patient",
                color: HexColor(HexColors.seaPink),
                function: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const RegisterPatientScreen()));
                },
              )
            ]),
      ),
    );
  }
}

class RegisterButton extends StatelessWidget {
  const RegisterButton(
      {Key? key,
      required this.title,
      required this.color,
      required this.function})
      : super(key: key);

  final String title;
  final Color color;
  final void Function()? function;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(10.r)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyleConstants.s20w700white,
              ),
              Container(
                height: 32.h,
                width: 32.w,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.r)),
                child: Icon(
                  Icons.arrow_forward_rounded,
                  color: color,
                  size: 22.sp,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
