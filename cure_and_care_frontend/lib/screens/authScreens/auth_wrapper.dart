import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/providers/auth_service_provider.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/screens/authScreens/register_screen.dart';
import 'package:cure_and_care_frontend/screens/doctorScreens/doctor_home_screen.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/patient_record_screen.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patient_home_screen.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class AuthWrapperScreen extends StatefulWidget {
  const AuthWrapperScreen({Key? key}) : super(key: key);

  @override
  State<AuthWrapperScreen> createState() => _AuthWrapperScreenState();
}

class _AuthWrapperScreenState extends State<AuthWrapperScreen> {
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      setUpEthereumContract();
    });
  }

  Future<void> setUpEthereumContract() async {
    var contractLinkingProvider =
        Provider.of<ContractLinkingProvider>(context, listen: false);
    var walletConnectProvider =
        Provider.of<WalletConnectProvider>(context, listen: false);
    var authServiceProvider =
        Provider.of<AuthServiceProvider>(context, listen: false);

    authServiceProvider.setContractLinkingProvider = contractLinkingProvider;
    authServiceProvider.setWalletConnectProvider = walletConnectProvider;

    await contractLinkingProvider
        .initialSetUp(walletConnectProvider.getConnector);

    await authServiceProvider.checkProfile(
        address: walletConnectProvider.getSessionStatus.accounts[0]);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Scaffold(
            backgroundColor: Colors.white,
            body: Center(
              child: LoadingBarWidget(
                isLoading: true,
                child: Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: HexColor(HexColors.pastelPink),
                  child: SafeArea(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 500.h,
                        ),
                        AnimatedTextKit(repeatForever: true, animatedTexts: [
                          TyperAnimatedText('Fetching Information.....',
                              textStyle: TextStyle(
                                  fontSize: 18.sp, color: Colors.black)),
                          TyperAnimatedText('Setting up your account....',
                              textStyle: TextStyle(
                                  fontSize: 18.sp, color: Colors.black)),
                          TyperAnimatedText('Hang Tight...',
                              textStyle: TextStyle(
                                  fontSize: 18.sp, color: Colors.black))
                        ])
                      ],
                    ),
                  ),
                ),
              ),
            ))
        : Scaffold(
            backgroundColor: Colors.white,
            body: Consumer<AuthServiceProvider>(builder: (context, auth, _) {
              switch (auth.getAuthStatus) {
                case AuthenticationStatus.unregistered:
                  return const RegisterationScreen();
                case AuthenticationStatus.registeredDoctor:
                  return const DoctorHomeScreen();
                case AuthenticationStatus.registeredPatient:
                  return const PatientHomeScreen();
                default:
                  return const Scaffold();
              }
            }),
          );
  }
}
