// ignore_for_file: prefer_final_fields

import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/providers/auth_service_provider.dart';
import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';

class RegisterDoctorScreen extends StatefulWidget {
  const RegisterDoctorScreen({Key? key}) : super(key: key);

  @override
  State<RegisterDoctorScreen> createState() => _RegisterDoctorScreenState();
}

class _RegisterDoctorScreenState extends State<RegisterDoctorScreen> {
  TextEditingController _fullNameTextEditingController =
      TextEditingController();
  TextEditingController _emailTextEditingController = TextEditingController();
  TextEditingController _dateTextEditingController = TextEditingController(
      text: DateFormat('dd-MM-yyyy').format(DateTime.now()));
  TextEditingController _registerationNumberTextEditingController =
      TextEditingController();
  TextEditingController _specialisationTextEditingController =
      TextEditingController();
  TextEditingController _phoneNumberTextEditingController =
      TextEditingController();
  bool _nameError = false;
  bool _emailError = false;
  DateTime _dateTime = DateTime.now();
  bool _registerationError = false;
  bool _specialisationError = false;
  bool _phoneError = false;

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthServiceProvider>(builder: (context, auth, _) {
      return LoadingBarWidget(
        isLoading: auth.isLoading,
        child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Colors.white,
              leading: IconButton(
                icon: Padding(
                  padding: EdgeInsets.only(left: 10.sp),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.black,
                    size: 22.sp,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            bottomNavigationBar: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 25.h),
              child: (_nameError ||
                      _emailError ||
                      _fullNameTextEditingController.text == "" ||
                      _emailTextEditingController.text == "" ||
                      _registerationError ||
                      _specialisationError ||
                      _registerationNumberTextEditingController.text == "" ||
                      _specialisationTextEditingController.text == "" ||
                      _phoneError ||
                      _phoneNumberTextEditingController.text == "" ||
                      auth.isLoading)
                  ? Container(
                      width: double.infinity,
                      height: 60.h,
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.w, vertical: 15.h),
                      decoration: BoxDecoration(
                          color: HexColor(HexColors.springWood),
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(10.r)),
                      child: Center(
                        child: Text(
                          "Register",
                          style: TextStyleConstants.s22w700grey,
                        ),
                      ),
                    )
                  : GestureDetector(
                      onTap: () async {
                        BigInt date =
                            BigInt.from(_dateTime.microsecondsSinceEpoch);

                        await auth.signUpDoctor(
                            name: _fullNameTextEditingController.text,
                            email: _emailTextEditingController.text,
                            dob: date,
                            phone: _phoneNumberTextEditingController.text,
                            regNo:
                                _registerationNumberTextEditingController.text,
                            specialisation:
                                _specialisationTextEditingController.text);
                        if (!mounted) return;
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: double.infinity,
                        height: 60.h,
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.w, vertical: 15.h),
                        decoration: BoxDecoration(
                            color: HexColor(HexColors.denimBlue),
                            borderRadius: BorderRadius.circular(10.r)),
                        child: Center(
                          child: Text(
                            "Register",
                            style: TextStyleConstants.s20w700white,
                          ),
                        ),
                      ),
                    ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 20.h),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Text(
                          "Create your account",
                          textAlign: TextAlign.center,
                          style: TextStyleConstants.s22w600seaPink,
                        ),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Text(
                        "Name *",
                        textAlign: TextAlign.start,
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextFormField(
                        // focusNode: textSecondFocusNode,
                        cursorColor: HexColor(HexColors.denimBlue),
                        autovalidateMode: AutovalidateMode.always,
                        controller: _fullNameTextEditingController,
                        keyboardType: TextInputType.name,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(0.0),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _nameError
                                    ? Colors.red
                                    : HexColor(HexColors.denimBlue)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _nameError ? Colors.red : Colors.grey),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _nameError ? Colors.red : Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color:
                                      _nameError ? Colors.red : Colors.grey)),
                          hintText: 'Enter your name',
                          hintStyle: TextStyleConstants.s14w400grey,
                          prefixIcon: Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 12.h,
                              horizontal: 0.w,
                            ),
                            child: _nameError
                                ? const Icon(
                                    Icons.person_outline,
                                    color: Colors.red,
                                  )
                                : const Icon(
                                    Icons.person_outline,
                                    color: Colors.grey,
                                  ),
                          ),
                        ),
                        textAlignVertical: TextAlignVertical.center,
                        textAlign: TextAlign.start,
                        inputFormatters: [
                          FilteringTextInputFormatter(RegExp(r'[a-zA-Z\s]'),
                              allow: true)
                        ],
                        onChanged: (v) {
                          if (v.length == 1 && v[0] == " ") {
                            _fullNameTextEditingController.text = '';
                          }

                          String removeTrailingAndLeading =
                              _fullNameTextEditingController.text.trim();

                          if (removeTrailingAndLeading.length < 3) {
                            setState(() {
                              _nameError = true;
                            });
                          } else {
                            setState(() {
                              _nameError = false;
                            });
                          }
                        },
                      ),
                      Visibility(
                        visible: _nameError,
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.h),
                          child: Text(
                            "Enter valid name",
                            style: TextStyleConstants.s12w400red,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Email *",
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextFormField(
                        cursorColor: HexColor(HexColors.denimBlue),
                        autovalidateMode: AutovalidateMode.always,
                        controller: _emailTextEditingController,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(0.0),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _emailError
                                    ? Colors.red
                                    : HexColor(HexColors.denimBlue)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _emailError ? Colors.red : Colors.grey),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _emailError ? Colors.red : Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color:
                                      _emailError ? Colors.red : Colors.grey)),
                          hintText: 'Enter your email',
                          hintStyle: TextStyleConstants.s14w400grey,
                          prefixIcon: Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 12.h,
                              horizontal: 0.w,
                            ),
                            child: _emailError
                                ? const Icon(
                                    Icons.mail_outline_outlined,
                                    color: Colors.red,
                                  )
                                : const Icon(
                                    Icons.mail_outline_outlined,
                                    color: Colors.grey,
                                  ),
                          ),
                        ),
                        textAlignVertical: TextAlignVertical.center,
                        textAlign: TextAlign.start,
                        onChanged: (v) {
                          if (EmailValidator.validate(v)) {
                            setState(() {
                              _emailError = false;
                            });
                          } else {
                            setState(() {
                              _emailError = true;
                            });
                          }
                        },
                      ),
                      Visibility(
                        visible: _emailError,
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.h),
                          child: Text(
                            "Enter valid email",
                            style: TextStyleConstants.s12w400red,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Date of Birth*",
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      GestureDetector(
                        onTap: () {
                          showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(DateTime.now().year - 100,
                                DateTime.now().month),
                            lastDate: DateTime.now(),
                          ).then((pickedDate) {
                            _dateTextEditingController.text =
                                DateFormat('dd-MM-yyyy').format(pickedDate!);
                            setState(() {
                              _dateTime = pickedDate;
                            });
                          });
                        },
                        child: TextFormField(
                          // focusNode: textSecondFocusNode,
                          cursorColor: HexColor(HexColors.denimBlue),
                          autovalidateMode: AutovalidateMode.always,
                          controller: _dateTextEditingController,
                          keyboardType: TextInputType.datetime,
                          textInputAction: TextInputAction.next,
                          enabled: false,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(0.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: HexColor(HexColors.denimBlue)),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: const BorderSide(color: Colors.grey),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: const BorderSide(color: Colors.grey),
                            ),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.r)),
                                borderSide:
                                    const BorderSide(color: Colors.grey)),
                            hintText: 'Enter your DOB',
                            hintStyle: TextStyleConstants.s14w400grey,
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 12.h,
                                horizontal: 0.w,
                              ),
                              child: const Icon(
                                Icons.calendar_month,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.start,

                          onChanged: (v) {},
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Mobile Number *",
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextFormField(
                        cursorColor: HexColor(HexColors.denimBlue),
                        autovalidateMode: AutovalidateMode.always,
                        controller: _phoneNumberTextEditingController,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.all(0.0),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _phoneError
                                    ? Colors.red
                                    : HexColor(HexColors.denimBlue)),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _phoneError ? Colors.red : Colors.grey),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(4.r)),
                            borderSide: BorderSide(
                                color: _phoneError ? Colors.red : Colors.grey),
                          ),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color:
                                      _phoneError ? Colors.red : Colors.grey)),
                          hintText: 'Enter your phone number',
                          hintStyle: TextStyleConstants.s14w400grey,
                          prefixIcon: Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 12.h,
                              horizontal: 0.w,
                            ),
                            child: _phoneError
                                ? const Icon(
                                    Icons.phone,
                                    color: Colors.red,
                                  )
                                : const Icon(
                                    Icons.phone,
                                    color: Colors.grey,
                                  ),
                          ),
                        ),
                        textAlignVertical: TextAlignVertical.center,
                        textAlign: TextAlign.start,
                        onChanged: (v) {
                          if (v.length == 10) {
                            setState(() {
                              _phoneError = false;
                            });
                          } else {
                            setState(() {
                              _phoneError = true;
                            });
                          }
                        },
                        inputFormatters: [
                          FilteringTextInputFormatter(RegExp(r'[0-9]'),
                              allow: true)
                        ],
                      ),
                      Visibility(
                        visible: _phoneError,
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.h),
                          child: Text(
                            "Enter valid phone number",
                            style: TextStyleConstants.s12w400red,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Specialisation *",
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextFormField(
                          cursorColor: HexColor(HexColors.denimBlue),
                          autovalidateMode: AutovalidateMode.always,
                          controller: _specialisationTextEditingController,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(0.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _specialisationError
                                      ? Colors.red
                                      : HexColor(HexColors.denimBlue)),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _specialisationError
                                      ? Colors.red
                                      : Colors.grey),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _specialisationError
                                      ? Colors.red
                                      : Colors.grey),
                            ),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.r)),
                                borderSide: BorderSide(
                                    color: _specialisationError
                                        ? Colors.red
                                        : Colors.grey)),
                            hintText: 'Enter your specialisation',
                            hintStyle: TextStyleConstants.s14w400grey,
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 12.h,
                                horizontal: 0.w,
                              ),
                              child: _specialisationError
                                  ? const Icon(
                                      Icons.medical_services,
                                      color: Colors.red,
                                    )
                                  : const Icon(
                                      Icons.medical_services,
                                      color: Colors.grey,
                                    ),
                            ),
                          ),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.start,
                          onChanged: (v) {
                            if (v.length == 1 && v[0] == " ") {
                              _specialisationTextEditingController.text = "";
                            }

                            if (_specialisationTextEditingController
                                .text.isEmpty) {
                              setState(() {
                                _specialisationError = true;
                              });
                            } else {
                              setState(() {
                                _specialisationError = false;
                              });
                            }
                          }),
                      Visibility(
                        visible: _specialisationError,
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.h),
                          child: Text(
                            "This field can't be empty",
                            style: TextStyleConstants.s12w400red,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Medical Registeration Number *",
                        style: TextStyleConstants.s16w400black,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextFormField(
                          cursorColor: HexColor(HexColors.denimBlue),
                          autovalidateMode: AutovalidateMode.always,
                          controller: _registerationNumberTextEditingController,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(0.0),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _registerationError
                                      ? Colors.red
                                      : HexColor(HexColors.denimBlue)),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _registerationError
                                      ? Colors.red
                                      : Colors.grey),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4.r)),
                              borderSide: BorderSide(
                                  color: _registerationError
                                      ? Colors.red
                                      : Colors.grey),
                            ),
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.r)),
                                borderSide: BorderSide(
                                    color: _emailError
                                        ? Colors.red
                                        : Colors.grey)),
                            hintText: 'Enter your registeration number',
                            hintStyle: TextStyleConstants.s14w400grey,
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: 12.h,
                                horizontal: 0.w,
                              ),
                              child: _registerationError
                                  ? const Icon(
                                      Icons.file_present,
                                      color: Colors.red,
                                    )
                                  : const Icon(
                                      Icons.file_present,
                                      color: Colors.grey,
                                    ),
                            ),
                          ),
                          textAlignVertical: TextAlignVertical.center,
                          textAlign: TextAlign.start,
                          onChanged: (v) {
                            if (v.length == 1 && v[0] == " ") {
                              _registerationNumberTextEditingController.text =
                                  "";
                            }

                            if (_registerationNumberTextEditingController
                                .text.isEmpty) {
                              setState(() {
                                _registerationError = true;
                              });
                            } else {
                              setState(() {
                                _registerationError = false;
                              });
                            }
                          }),
                      Visibility(
                        visible: _registerationError,
                        child: Padding(
                          padding: EdgeInsets.only(top: 5.h),
                          child: Text(
                            "This field can't be empty",
                            style: TextStyleConstants.s12w400red,
                          ),
                        ),
                      ),
                    ]),
              ),
            )),
      );
    });
  }
}
