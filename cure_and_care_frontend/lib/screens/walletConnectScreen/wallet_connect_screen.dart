import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/screens/walletConnectScreen/carouselWidget/carousel_widget.dart';
import 'package:cure_and_care_frontend/screens/walletConnectScreen/scan_qr_code_widget.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class WalletConnectScreen extends StatefulWidget {
  const WalletConnectScreen({Key? key}) : super(key: key);

  @override
  State<WalletConnectScreen> createState() => _WalletConnectScreenState();
}

class _WalletConnectScreenState extends State<WalletConnectScreen> {
  @override
  Widget build(BuildContext context) {
    var walletProvider = Provider.of<WalletConnectProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,

        //TODO: Add logo instead of text

        title: Text(
          "Cure & Care",
          style: TextStyleConstants.s22w600black,
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const WalletConnectCarouselWidget(),
              SizedBox(
                height: 20.h,
              ),
              Text(
                "Connect to metamask wallet",
                style: TextStyleConstants.s13w400black87,
              ),
              SizedBox(
                height: 10.h,
              ),
              GestureDetector(
                onTap: () async {
                  walletProvider.setBuildContext = context;
                  walletProvider.setIsUsingQRCode = false;
                  await _launchUrl(walletProvider.uri);
                },
                child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.r),
                        color: HexColor(HexColors.pastelPink)),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/images/logos/metamask_logo.png",
                          width: 40.w,
                          fit: BoxFit.fill,
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              "Redirect to Metamask Wallet",
                              style: TextStyleConstants.s16w500black,
                            ),
                          ),
                        )
                      ],
                    )),
              ),
              SizedBox(
                height: 5.h,
              ),
              Text(
                "OR",
                style: TextStyleConstants.s13w400black87,
              ),
              SizedBox(
                height: 5.h,
              ),
              const ScanQRCodeWidget(),
            ],
          ),
        ),
      )),
    );
  }

  Future<void> _launchUrl(String _url) async {
    Uri url = Uri.parse("metamask://wc?uri=" + _url);
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      print("Can't launch");
    }
  }
}
