import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

class ScanQRCodeWidget extends StatelessWidget {
  const ScanQRCodeWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var walletProvider = Provider.of<WalletConnectProvider>(context);
    return GestureDetector(
      onTap: () {
        walletProvider.setBuildContext = context;
        walletProvider.setIsUsingQRCode = true;
        showModalBottomSheet(
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(20.r),
                topLeft: Radius.circular(20.r),
              ),
            ),
            context: context,
            builder: (context) {
              return Wrap(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
                    child: Center(
                      child: SizedBox(
                          height: 250.h,
                          width: 250.w,
                          child: QrImage(data: walletProvider.uri)),
                    ),
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 50.w, vertical: 20.h),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.r),
                            color: HexColor(HexColors.bluishCyan)),
                        child: Text(
                          "Cancel",
                          style: TextStyleConstants.s16w500black,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  )
                ],
              );
            });
      },
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.r),
              color: HexColor(HexColors.pastelPink)),
          child: Row(
            children: [
              Image.asset(
                "assets/images/scan_qr_code.png",
                width: 40.w,
                fit: BoxFit.fill,
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "Scan QR Code",
                    style: TextStyleConstants.s16w500black,
                  ),
                ),
              ),
              SizedBox(
                width: 40.w,
              )
            ],
          )),
    );
  }
}
