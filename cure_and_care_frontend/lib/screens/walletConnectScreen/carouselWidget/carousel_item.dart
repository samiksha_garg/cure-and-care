import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

class WalletConnectCarouselItem extends StatelessWidget {
  const WalletConnectCarouselItem(
      {Key? key, required this.image, required this.title})
      : super(key: key);

  final String image;
  final String title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              image,
              height: 220.h,
              fit: BoxFit.fill,
            ),
            SizedBox(
              height: 20.h,
            ),
            Text(title,
                textAlign: TextAlign.center,
                style: TextStyleConstants.s16w600black)
          ],
        ),
      ),
    );
  }
}
