import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WalletConnectCarouselSlider extends StatelessWidget {
  const WalletConnectCarouselSlider(
      {Key? key, required this.index, required this.currentIndex})
      : super(key: key);

  final int index;
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 5.w,
      height: 5.h,
      margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: index == currentIndex
            ? const Color.fromRGBO(0, 0, 0, 0.9)
            : const Color.fromRGBO(0, 0, 0, 0.4),
      ),
    );
  }
}
