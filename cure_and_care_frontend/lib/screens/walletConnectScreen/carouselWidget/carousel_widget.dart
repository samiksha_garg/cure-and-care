import 'package:carousel_slider/carousel_slider.dart';
import 'package:cure_and_care_frontend/screens/walletConnectScreen/carouselWidget/carousel_slider.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WalletConnectCarouselWidget extends StatefulWidget {
  const WalletConnectCarouselWidget({Key? key}) : super(key: key);

  @override
  State<WalletConnectCarouselWidget> createState() =>
      _WalletConnectCarouselWidgetState();
}

class _WalletConnectCarouselWidgetState
    extends State<WalletConnectCarouselWidget> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CarouselSlider(
          items: WalletConnectCarousel.items,
          options: CarouselOptions(
              viewportFraction: 1,
              height: 330.h,
              autoPlay: true,
              scrollDirection: Axis.horizontal,
              enlargeCenterPage: true,
              onPageChanged: (index, reason) {
                setState(() {
                  currentIndex = index;
                });
              }),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            WalletConnectCarouselSlider(index: 0, currentIndex: currentIndex),
            WalletConnectCarouselSlider(index: 1, currentIndex: currentIndex),
            WalletConnectCarouselSlider(index: 2, currentIndex: currentIndex)
          ],
        ),
      ],
    );
  }
}
