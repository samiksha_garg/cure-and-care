import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/patient_records_body.dart';
import 'package:cure_and_care_frontend/services/file_picker_service.dart';
import 'package:cure_and_care_frontend/services/image_picker_service.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../providers/patient_records_provider.dart';

class PatientRecordScreen extends StatefulWidget {
  const PatientRecordScreen({Key? key}) : super(key: key);

  @override
  State<PatientRecordScreen> createState() => _PatientRecordScreenState();
}

class _PatientRecordScreenState extends State<PatientRecordScreen> {
  bool fetchingDoc = false;
  @override
  void initState() {
    // Future.delayed(Duration.zero, () {
    //   getPatientDetails();
    // });
    super.initState();
  }

  Future<void> getPatientDetails() async {
    var patientRecordProvider =
        Provider.of<PatientRecordsProvider>(context, listen: false);

    if (mounted) {
      await patientRecordProvider.fetchPatientReports(context);
    }
  }

  Future<void> uploadPDF() async {
    setState(() {
      fetchingDoc = true;
    });
    String cId = await FilePickerService.uploadFile();
    print(cId);
    setState(() {
      fetchingDoc = false;
    });

    TextEditingController fileNameController = TextEditingController();
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return SimpleDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.r))),
            title: Text(
              "Enter file-name",
              textAlign: TextAlign.center,
              style: TextStyleConstants.s16w600black,
            ),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                child: TextFormField(
                  // focusNode: textSecondFocusNode,
                  cursorColor: HexColor(HexColors.denimBlue),
                  autovalidateMode: AutovalidateMode.always,
                  controller: fileNameController,
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(0.0),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide:
                          BorderSide(color: HexColor(HexColors.denimBlue)),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide: const BorderSide(color: Colors.grey),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide: const BorderSide(color: Colors.grey),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4.r)),
                        borderSide: const BorderSide(color: Colors.grey)),
                    hintText: 'Enter file name',
                    hintStyle: TextStyleConstants.s14w400grey,
                    prefixIcon: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 12.h,
                        horizontal: 0.w,
                      ),
                      child: const Icon(
                        Icons.drive_file_rename_outline_outlined,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  textAlignVertical: TextAlignVertical.center,
                  textAlign: TextAlign.start,

                  onChanged: (v) {
                    if (v.length == 1 && v[0] == " ") {
                      fileNameController.text = '';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                child: Text(
                  "* default name would be saved as Untitled",
                  style: TextStyleConstants.s13w400black87,
                ),
              ),
              SizedBox(
                height: 15.h,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                    decoration: BoxDecoration(
                        color: HexColor(HexColors.seaPink),
                        borderRadius: BorderRadius.circular(15.r)),
                    child: Text(
                      "Submit",
                      style: TextStyleConstants.s16w700white,
                    ),
                  ),
                ),
              )
            ],
          );
        });

    if (mounted) {
      var patientRecordProvider =
          Provider.of<PatientRecordsProvider>(context, listen: false);
      BigInt doc = BigInt.from(DateTime.now().microsecondsSinceEpoch);
      await patientRecordProvider.addPatientRecord(
          buildContext: context,
          doc: doc,
          name: fileNameController.text == ""
              ? "Untitled"
              : fileNameController.text,
          cId: cId,
          isPdf: true);
      await getPatientDetails();
    }
  }

  Future<void> uploadImage() async {
    setState(() {
      fetchingDoc = true;
    });
    String cId = await ImagePickerService.uploadImage();

    setState(() {
      fetchingDoc = false;
    });

    TextEditingController fileNameController = TextEditingController();
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return SimpleDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.r))),
            title: Text(
              "Enter file-name",
              textAlign: TextAlign.center,
              style: TextStyleConstants.s16w600black,
            ),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                child: TextFormField(
                  // focusNode: textSecondFocusNode,
                  cursorColor: HexColor(HexColors.denimBlue),
                  autovalidateMode: AutovalidateMode.always,
                  controller: fileNameController,
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(0.0),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide:
                          BorderSide(color: HexColor(HexColors.denimBlue)),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide: const BorderSide(color: Colors.grey),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.r)),
                      borderSide: const BorderSide(color: Colors.grey),
                    ),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4.r)),
                        borderSide: const BorderSide(color: Colors.grey)),
                    hintText: 'Enter file name',
                    hintStyle: TextStyleConstants.s14w400grey,
                    prefixIcon: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 12.h,
                        horizontal: 0.w,
                      ),
                      child: const Icon(
                        Icons.drive_file_rename_outline_outlined,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  textAlignVertical: TextAlignVertical.center,
                  textAlign: TextAlign.start,

                  onChanged: (v) {
                    if (v.length == 1 && v[0] == " ") {
                      fileNameController.text = '';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                child: Text(
                  "* default name would be saved as Untitled",
                  style: TextStyleConstants.s13w400black87,
                ),
              ),
              SizedBox(
                height: 15.h,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.w, vertical: 15.h),
                    decoration: BoxDecoration(
                        color: HexColor(HexColors.seaPink),
                        borderRadius: BorderRadius.circular(15.r)),
                    child: Text(
                      "Submit",
                      style: TextStyleConstants.s16w700white,
                    ),
                  ),
                ),
              )
            ],
          );
        });

    if (mounted) {
      var patientRecordProvider =
          Provider.of<PatientRecordsProvider>(context, listen: false);
      BigInt doc = BigInt.from(DateTime.now().microsecondsSinceEpoch);
      await patientRecordProvider.addPatientRecord(
          buildContext: context,
          doc: doc,
          name: fileNameController.text == ""
              ? "Untitled"
              : fileNameController.text,
          cId: cId,
          isPdf: false);
      await getPatientDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PatientRecordsProvider>(builder: (context, records, _) {
      return LoadingBarWidget(
        isLoading: records.isLoading || fetchingDoc,
        child: Scaffold(
          floatingActionButton: GestureDetector(
            onTap: () async {
              await showDialog(
                  context: context,
                  builder: (context) {
                    return SimpleDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(15.r))),
                      title: Text(
                        "Type of record to be added",
                        textAlign: TextAlign.center,
                        style: TextStyleConstants.s16w600black,
                      ),
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20.w),
                          child: GestureDetector(
                            onTap: () async {
                              uploadPDF();
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10.w, vertical: 20.h),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: HexColor(HexColors.seaPink),
                                  borderRadius: BorderRadius.circular(10.r)),
                              child: Center(
                                child: Text(
                                  "PDF",
                                  style: TextStyleConstants.s16w700white,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20.w),
                          child: GestureDetector(
                            onTap: () async {
                              uploadImage();
                              Navigator.pop(context);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10.w, vertical: 20.h),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: HexColor(HexColors.denimBlue),
                                  borderRadius: BorderRadius.circular(10.r)),
                              child: Center(
                                child: Text(
                                  "Image",
                                  style: TextStyleConstants.s16w700white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            },
            child: CircleAvatar(
              backgroundColor: HexColor(HexColors.seaPink),
              radius: 35.r,
              child: Center(
                  child: Icon(
                Icons.add,
                size: 35.sp,
                color: HexColor(HexColors.pastelPink),
              )),
            ),
          ),
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            title: Text(
              "Your Records",
              style: TextStyleConstants.s22w600black,
            ),
          ),
          body: PatientRecordsBody(),
        ),
      );
    });
  }
}
