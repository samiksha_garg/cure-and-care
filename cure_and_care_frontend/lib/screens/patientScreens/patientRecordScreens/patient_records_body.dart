import 'package:cure_and_care_frontend/models/record_model.dart';
import 'package:cure_and_care_frontend/providers/patient_records_provider.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/patient_record_tile.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class PatientRecordsBody extends StatelessWidget {
  const PatientRecordsBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<PatientRecordsProvider>(builder: (context, records, _) {
      return ListView.builder(
          itemCount: records.getReports.length,
          itemBuilder: (context, int index) {
            return PatientRecordTile(
              record: records.getReports[index],
            );
          });
    });
  }
}
