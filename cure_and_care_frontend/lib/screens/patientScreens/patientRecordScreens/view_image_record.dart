import 'package:cached_network_image/cached_network_image.dart';
import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ViewImageRecord extends StatelessWidget {
  const ViewImageRecord({Key? key, required this.cId, required this.title})
      : super(key: key);

  final String cId;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: Padding(
              padding: EdgeInsets.only(left: 10.sp),
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
                size: 22.sp,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            title,
            style: TextStyleConstants.s16w600black,
          ),
        ),
        body: Center(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.w),
              child: CachedNetworkImage(
                imageUrl: "https://ipfs.io/ipfs/$cId",
                placeholder: (context, url) => Container(
                  height: double.infinity,
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(vertical: 20.h),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // color: Color(0xFF4A3298),
                    borderRadius: BorderRadius.circular(20.r),
                  ),
                  child: Center(
                      child: LoadingBarWidget(
                    isLoading: true,
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Colors.white,
                    ),
                  )),
                ),
                errorWidget: (context, url, error) => Icon(
                  Icons.error,
                  size: 20.sp,
                  color: Colors.red,
                ),
              )),
        ));
  }
}
