import 'dart:io';

import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class ViewPDFRecord extends StatefulWidget {
  const ViewPDFRecord({Key? key, required this.title, required this.cId})
      : super(key: key);
  final String title;
  final String cId;

  @override
  State<ViewPDFRecord> createState() => _ViewPDFRecordState();
}

class _ViewPDFRecordState extends State<ViewPDFRecord> {
  late File Pfile;
  bool isLoading = true;

  @override
  void initState() {
    loadNetwork();
    super.initState();
  }

  Future<void> loadNetwork() async {
    String cId = widget.cId;
    var url = 'https://ipfs.io/ipfs/$cId';
    final response = await http.get(Uri.parse(url));
    final bytes = response.bodyBytes;
    final filename = url.substring(url.lastIndexOf("/") + 1);
    final dir = await getApplicationDocumentsDirectory();
    var file = File('${dir.path}/$filename');
    await file.writeAsBytes(bytes, flush: true);
    setState(() {
      Pfile = file;
    });

    print(Pfile);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LoadingBarWidget(
      isLoading: isLoading,
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Padding(
                padding: EdgeInsets.only(left: 10.sp),
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                  size: 22.sp,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              widget.title,
              style: TextStyleConstants.s16w600black,
            ),
          ),
          body: Center(
              child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: isLoading
                ? Container()
                : PDFView(
                    filePath: Pfile.path,
                  ),
          ))),
    );
  }
}
