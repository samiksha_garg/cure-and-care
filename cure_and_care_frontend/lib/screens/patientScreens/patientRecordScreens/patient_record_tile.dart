import 'package:cure_and_care_frontend/models/record_model.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/view_image_record.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/view_pdf_record.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class PatientRecordTile extends StatelessWidget {
  const PatientRecordTile({Key? key, required this.record}) : super(key: key);

  final Record record;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => record.isPdf
                      ? ViewPDFRecord(
                          title: record.name,
                          cId: record.recordHash,
                        )
                      : ViewImageRecord(
                          cId: record.recordHash,
                          title: record.name,
                        )));
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 15.h),
          decoration: BoxDecoration(
              color: !record.isPdf
                  ? HexColor(HexColors.pastelPink)
                  : HexColor(HexColors.bluishCyan),
              borderRadius: BorderRadius.circular(15.r)),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  record.name,
                  style: TextStyleConstants.s16w500black,
                ),
                SizedBox(
                  height: 5.h,
                ),
                Text(DateFormat('dd-MM-yyyy')
                    .format(record.dateOfCreation)
                    .trim())
              ],
            ),
            Icon(
              !record.isPdf ? Icons.insert_photo : Icons.file_copy_sharp,
              size: 30.sp,
              color: Colors.black,
            )
          ]),
        ),
      ),
    );
  }
}
