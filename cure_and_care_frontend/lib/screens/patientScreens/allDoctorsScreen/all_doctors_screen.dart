import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/providers/all_doctors_provider.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/allDoctorsScreen/doctor_tile.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../utils/constants.dart';

class AllDoctorsScreen extends StatefulWidget {
  const AllDoctorsScreen({Key? key}) : super(key: key);

  @override
  State<AllDoctorsScreen> createState() => _AllDoctorsScreenState();
}

class _AllDoctorsScreenState extends State<AllDoctorsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            "Doctors",
            style: TextStyleConstants.s22w600black,
          ),
        ),
        body: Consumer<AllDoctorsProvider>(builder: (context, allDoctors, _) {
          return LoadingBarWidget(
              isLoading: allDoctors.isLoading,
              child: GridView.builder(
                  itemCount: allDoctors.getAllDoctors.length,
                  padding: EdgeInsets.only(left: 10.w, top: 10.h),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 0.8),
                  itemBuilder: (context, position) {
                    return DoctorTile(
                      doctorModel: allDoctors.getAllDoctors[position],
                    );
                  }));
        }));
  }
}
