import 'package:cure_and_care_frontend/models/doctor_model.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../utils/constants.dart';

class DoctorTile extends StatelessWidget {
  const DoctorTile({Key? key, required this.doctorModel}) : super(key: key);

  final DoctorModel doctorModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 10.w),
      child: Container(
        width: 180.w,
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 20.h),
        decoration: BoxDecoration(
            color: HexColor(HexColors.pastelPink),
            borderRadius: BorderRadius.circular(10.r)),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          CircleAvatar(
            radius: 50.r,
            backgroundImage: AssetImage("assets/images/doctor-profile.jpg"),
          ),
          SizedBox(
            height: 15.h,
          ),
          Text(
            doctorModel.name,
            style: TextStyleConstants.s22w600black,
          ),
          SizedBox(
            height: 5.h,
          ),
          Text(
            doctorModel.specialisation,
            style: TextStyleConstants.s15w500black,
          )
        ]),
      ),
    );
  }
}
