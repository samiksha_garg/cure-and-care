import 'package:cure_and_care_frontend/components/custom_navigation_bar.dart';
import 'package:cure_and_care_frontend/models/patient_model.dart';
import 'package:cure_and_care_frontend/providers/all_doctors_provider.dart';
import 'package:cure_and_care_frontend/providers/bottom_navigation_bar.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:cure_and_care_frontend/providers/patient_records_provider.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/allDoctorsScreen/all_doctors_screen.dart';
import 'package:cure_and_care_frontend/screens/patientScreens/patientRecordScreens/patient_record_screen.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/near_pharma_screen.dart';
import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_service_provider.dart';
import '../../providers/wallet_connect_provider.dart';

class PatientHomeScreen extends StatefulWidget {
  const PatientHomeScreen({Key? key}) : super(key: key);

  @override
  State<PatientHomeScreen> createState() => _PatientHomeScreenState();
}

class _PatientHomeScreenState extends State<PatientHomeScreen> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getDetails();
    });
    super.initState();
  }

  Future<void> getDetails() async {
    var walletConnectProvider =
        Provider.of<WalletConnectProvider>(context, listen: false);
    var authServiceProvider =
        Provider.of<AuthServiceProvider>(context, listen: false);
    var patientRecordProvider =
        Provider.of<PatientRecordsProvider>(context, listen: false);
    var allDoctorsProvider =
        Provider.of<AllDoctorsProvider>(context, listen: false);

    await authServiceProvider
        .getPatientDetails(walletConnectProvider.getSessionStatus.accounts[0]);

    if (mounted) {
      await patientRecordProvider.fetchPatientReports(context);
    }

    if (mounted) {
      await allDoctorsProvider.fetchAllDoctors(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomNavigationBar(),
      body: Consumer<BottomNavigationBarProvider>(
          builder: (context, bottomNav, _) {
        switch (bottomNav.currentPage) {
          case PatientMenuStatus.doctorScreen:
            return const AllDoctorsScreen();
          case PatientMenuStatus.recordScreen:
            return const PatientRecordScreen();
          case PatientMenuStatus.pharmacyScreen:
            return const NearPharmaScreen();
          default:
            return const AllDoctorsScreen();
        }
      }),
    );
  }
}
