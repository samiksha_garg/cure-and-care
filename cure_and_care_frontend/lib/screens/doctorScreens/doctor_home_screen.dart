import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_service_provider.dart';

class DoctorHomeScreen extends StatefulWidget {
  const DoctorHomeScreen({Key? key}) : super(key: key);

  @override
  State<DoctorHomeScreen> createState() => _DoctorHomeScreenState();
}

class _DoctorHomeScreenState extends State<DoctorHomeScreen> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getDoctorDetails();
    });
    super.initState();
  }

  Future<void> getDoctorDetails() async {
    var walletConnectProvider =
        Provider.of<WalletConnectProvider>(context, listen: false);
    var authServiceProvider =
        Provider.of<AuthServiceProvider>(context, listen: false);

    await authServiceProvider
        .getDoctorDetails(walletConnectProvider.getSessionStatus.accounts[0]);
    if (kDebugMode) {
      print(authServiceProvider.getDoctorModel.name);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
