import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cure_and_care_frontend/components/loadingBar.dart';
import 'package:cure_and_care_frontend/providers/all_doctors_provider.dart';
import 'package:cure_and_care_frontend/providers/auth_service_provider.dart';
import 'package:cure_and_care_frontend/providers/bottom_navigation_bar.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:cure_and_care_frontend/providers/patient_records_provider.dart';
import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/screens/authScreens/auth_wrapper.dart';
import 'package:cure_and_care_frontend/screens/authScreens/register_screen.dart';

import 'package:cure_and_care_frontend/screens/walletConnectScreen/wallet_connect_screen.dart';
import 'package:cure_and_care_frontend/services/ipfs_service.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:cure_and_care_frontend/utils/theme.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:cure_and_care_frontend/screens/pharmaScreen/near_pharma_screen.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<WalletConnectProvider>(
          create: (context) => WalletConnectProvider(),
        ),
        ChangeNotifierProvider<ContractLinkingProvider>(
          create: (context) => ContractLinkingProvider(),
        ),
        ChangeNotifierProvider<AuthServiceProvider>(
          create: (context) => AuthServiceProvider(),
        ),
        ChangeNotifierProvider<PatientRecordsProvider>(
          create: (context) => PatientRecordsProvider(),
        ),
        ChangeNotifierProvider<AllDoctorsProvider>(
          create: (context) => AllDoctorsProvider(),
        ),
        ChangeNotifierProvider<BottomNavigationBarProvider>(
          create: (context) => BottomNavigationBarProvider(),
        ),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Cure And Care',
          theme: themeData,
          home: Builder(
              builder: (context) => ScreenUtilInit(
                    builder: (context, child) {
                      return Consumer<WalletConnectProvider>(
                        builder: (context, walletConnect, _) {
                          switch (walletConnect.getWalletConnectStatus) {
                            case WalletConnectStatus.uninitialized:
                              return Scaffold(
                                backgroundColor: Colors.white,
                                body: Center(
                                  child: LoadingBarWidget(
                                    isLoading: true,
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      color: HexColor(HexColors.pastelPink),
                                    ),
                                  ),
                                ),
                              );
                            case WalletConnectStatus.initiated:
                              return const WalletConnectScreen();
                            case WalletConnectStatus.connected:
                              return const AuthWrapperScreen();
                            default:
                              return Scaffold(
                                backgroundColor: Colors.white,
                                body: Center(
                                  child: LoadingBarWidget(
                                    isLoading: true,
                                    child: Container(
                                      width: double.infinity,
                                      height: double.infinity,
                                      color: HexColor(HexColors.pastelPink),
                                    ),
                                  ),
                                ),
                              );
                          }
                        },
                      );
                    },
                    designSize: Size(375,
                        MediaQuery.of(context).size.height > 720 ? 815 : 700),
                  ))),
    );
  }
}
