import 'dart:ui';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class HexColors {
  static String bluishCyan = "#E6F5FA";
  static String denimBlue = "#74C1DB";
  static String pastelPink = "#FED4D6";
  static String seaPink = "#F89397";
  static String springWood = "#F6F6F4";
  static String black = "#000000";
  static String white = "#FFFFFF";
  static String inactiveColor = "#B6B6B6";
}
