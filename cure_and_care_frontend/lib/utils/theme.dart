import 'package:cure_and_care_frontend/utils/constants.dart';
import 'package:flutter/material.dart';

ThemeData themeData = ThemeData(
  scaffoldBackgroundColor: Colors.white,
  textTheme: textTheme,
  inputDecorationTheme: inputDecorationTheme,
  fontFamily: 'WorkSans',
);

InputDecorationTheme inputDecorationTheme = InputDecorationTheme();
TextTheme textTheme = TextTheme(
    bodyText1: TextStyle(color: ColorConstants.textColor),
    bodyText2: TextStyle(color: ColorConstants.textColor));
