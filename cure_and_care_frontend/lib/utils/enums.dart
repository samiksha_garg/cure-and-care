enum WalletConnectStatus {
  uninitialized,
  connected,
  initiated,
  disconnected,
  updated
}

enum AuthenticationStatus { unregistered, registeredDoctor, registeredPatient }

enum PatientMenuStatus { doctorScreen, recordScreen, pharmacyScreen }
