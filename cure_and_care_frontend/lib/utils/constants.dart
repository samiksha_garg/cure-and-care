import 'package:cure_and_care_frontend/screens/walletConnectScreen/carouselWidget/carousel_item.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ColorConstants {
  static Color textColor = HexColor(HexColors.black);
}

class WalletConnectCarousel {
  static String reportsContent =
      "Securely store your personal medical records.";
  static String reportsImage = "assets/images/carousel_reports.svg";

  static String medicineContent =
      "Easily connect to nearby/online medicine stores.";
  static String medicineImage = "assets/images/carousel_medicine.svg";

  static String doctorContent =
      "Connect and schedule appointments with experienced doctors";
  static String doctorImage = "assets/images/carousel_doctor.svg";

  static List<WalletConnectCarouselItem> items = [
    WalletConnectCarouselItem(image: doctorImage, title: doctorContent),
    WalletConnectCarouselItem(image: medicineImage, title: medicineContent),
    WalletConnectCarouselItem(image: reportsImage, title: reportsContent),
  ];
}

abstract class TextStyleConstants {
  //black
  static TextStyle s15wblack = TextStyle(
    fontFamily: 'WorkSans',
    color: HexColor(HexColors.black),
    fontSize: 15.sp,
  );

  static TextStyle s15w500black = TextStyle(
    fontFamily: 'WorkSans',
    color: HexColor(HexColors.black),
    fontSize: 15.sp,
    height: 1.2,
    fontWeight: FontWeight.w500,
  );

  static TextStyle s16w600black = TextStyle(
      color: HexColor(HexColors.black),
      fontFamily: "WorkSans",
      fontSize: 16.sp,
      fontWeight: FontWeight.w600);

  static TextStyle s22w600black = TextStyle(
      fontFamily: "WorkSans",
      color: Colors.black,
      fontWeight: FontWeight.w600,
      fontSize: 22.sp);

  static TextStyle s16w500black = TextStyle(
      fontWeight: FontWeight.w500, fontSize: 16.sp, fontFamily: "WorkSans");

  static TextStyle s22w700black = TextStyle(
      color: Colors.black,
      fontFamily: "WorkSans",
      fontSize: 22.sp,
      fontWeight: FontWeight.w700);

  static TextStyle s16w400black = TextStyle(
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
      fontFamily: "WorkSans",
      color: Colors.black);

  //white
  static TextStyle s20w700white = TextStyle(
      color: Colors.white,
      fontSize: 20.sp,
      fontWeight: FontWeight.w700,
      fontFamily: "WorkSans");

  static TextStyle s16w700white = TextStyle(
      color: Colors.white,
      fontSize: 16.sp,
      fontWeight: FontWeight.w700,
      fontFamily: "WorkSans");

  //grey
  static TextStyle s14w400grey = TextStyle(
      fontFamily: "WorkSans",
      fontSize: 14.sp,
      fontWeight: FontWeight.w400,
      color: Colors.grey);

  static TextStyle s22w700grey = TextStyle(
      fontFamily: "WorkSans",
      fontSize: 22.sp,
      fontWeight: FontWeight.w700,
      color: Colors.grey);

  //black87

  static TextStyle s13w400black87 = TextStyle(
      color: Colors.black87,
      fontSize: 13.sp,
      fontFamily: "WorkSans",
      fontWeight: FontWeight.w400);

  //red
  static TextStyle s12w400red = TextStyle(
      fontSize: 12.sp,
      color: Colors.red,
      fontWeight: FontWeight.w400,
      fontFamily: "WorkSans");

  //denim-blue
  static TextStyle s22w600denimBlue = TextStyle(
      fontSize: 22.sp,
      fontFamily: "WorkSans",
      fontWeight: FontWeight.w600,
      color: HexColor(HexColors.denimBlue));

  //sea-pink
  static TextStyle s22w600seaPink = TextStyle(
      fontSize: 22.sp,
      fontFamily: "WorkSans",
      fontWeight: FontWeight.w600,
      color: HexColor(HexColors.seaPink));
}
