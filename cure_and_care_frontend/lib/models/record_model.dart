class Record {
  final String name;
  final String recordHash;
  final DateTime dateOfCreation;
  final bool isPdf;

  Record(
      {required this.name,
      required this.recordHash,
      required this.dateOfCreation,
      required this.isPdf});

  factory Record.fromArray(dynamic array, bool isPdf) {
    return Record(
        name: array[1],
        recordHash: array[0],
        dateOfCreation: DateTime.fromMicrosecondsSinceEpoch(array[2].toInt()),
        isPdf: isPdf);
  }
}
