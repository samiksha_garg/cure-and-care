import 'package:cure_and_care_frontend/screens/PharmaScreen/pharmacy_type.dart';
class PharmacyModel {
  final String pharmacyName;
  final String pharmacyOwnerId;
  final double pharmacyLatitude;
  final double pharmacyLongitude;
  final List<dynamic> pharmacyDrugs;
  // final String pharmacyOwnerName;
  // final String pharmacyPictureUrl;
  // final PharmacyType pharmacyType;
  PharmacyModel({
    required this.pharmacyName,
    required this.pharmacyOwnerId,
    required this.pharmacyLatitude,
    required this.pharmacyLongitude,
    required this.pharmacyDrugs,
    // required this.pharmacyOwnerName,
    // required this.pharmacyPictureUrl,
    // required this.pharmacyType
});
}