class DoctorModel {
  final String name;
  final String specialisation;
  final String registerationNumber;
  final String email;
  final DateTime dateOfBirth;
  final String phoneNo;
  final String ethAddress;

  DoctorModel(
      {required this.name,
      required this.specialisation,
      required this.registerationNumber,
      required this.email,
      required this.dateOfBirth,
      required this.phoneNo,
      required this.ethAddress});

  factory DoctorModel.fromArray(dynamic array) {
    return DoctorModel(
        name: array[0],
        ethAddress: array[1].toString(),
        email: array[2],
        dateOfBirth: DateTime.fromMicrosecondsSinceEpoch(array[3].toInt()),
        specialisation: array[4],
        registerationNumber: array[5],
        phoneNo: array[6]);
  }
}
