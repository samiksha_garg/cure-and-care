class PatientModel {
  final String name;

  final String email;
  final DateTime dateOfBirth;
  final String phoneNo;

  PatientModel(
      {required this.name,
      required this.email,
      required this.dateOfBirth,
      required this.phoneNo});
}
