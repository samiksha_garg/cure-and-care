import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class IpfsService {
  Future<String> uploadToIpfs(/*String uploadPath,*/ Uint8List bytes) async {
    try {
      //final bytes = File(uploadPath).readAsBytesSync();

      String addressAbiStringFile =
          await rootBundle.loadString("secrets/nft_storage.json");
      var jsonAddress = jsonDecode(addressAbiStringFile);
      var apiKey = jsonAddress["nft_storage"];

      final response = await http.post(
        Uri.parse('https://api.nft.storage/upload'),
        headers: {
          'Authorization': 'Bearer $apiKey',
          'content-type': 'images/*'
        },
        body: bytes,
      );

      final data = jsonDecode(response.body);

      final cid = data['value']['cid'];

      return cid;
    } catch (e) {
      if (kDebugMode) {
        print('Error at IPFS Service - uploadImage: $e');
      }

      rethrow;
    }
  }
}
