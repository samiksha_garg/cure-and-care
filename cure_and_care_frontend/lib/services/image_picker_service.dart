import 'dart:typed_data';

import 'package:cure_and_care_frontend/services/ipfs_service.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerService {
  static Future<String> uploadImage() async {
    final ImagePicker picker = ImagePicker();

    try {
      // Pick an images
      final XFile? image = await picker.pickImage(
        source: ImageSource.gallery,
      );

      //Nothing picked
      if (image == null) {
        return "No image selected";
      } else {
        final Uint8List bytes = await image.readAsBytes();

        // upload images to ipfs
        final cid = await IpfsService().uploadToIpfs(bytes);

        return cid;
      }
    } catch (e) {
      if (kDebugMode) {
        print('Error at images picker: $e');
      }

      return "Error at image picker";
    }
  }
}
