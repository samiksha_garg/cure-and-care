import 'package:cure_and_care_frontend/services/ipfs_service.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';

class FilePickerService {
  static Future<String> uploadFile() async {
    try {
      // Pick an file
      FilePickerResult? file = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        withData: true,
        type: FileType.custom,
        allowedExtensions: ['pdf'],
      );

      //Nothing picked
      if (file == null) {
        if (kDebugMode) {
          print("No file selected");
        }

        return "No file selected";
      } else {
        final bytes = file.files.single.bytes!;
        // upload video to ipfs
        final cid =
            await IpfsService().uploadToIpfs(/*file.files.single.path!*/ bytes);
        return cid;
      }
    } catch (e) {
      if (kDebugMode) {
        print('Error at file picker: $e');
      }

      return "Error at file picker";
    }
  }
}
