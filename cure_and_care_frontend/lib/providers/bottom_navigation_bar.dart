import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/material.dart';

class BottomNavigationBarProvider with ChangeNotifier {
  int _currentindex = 0;
  PatientMenuStatus _patientMenuStatus = PatientMenuStatus.doctorScreen;

  get currentIndex => _currentindex;

  get currentPage => _patientMenuStatus;

  void toggleTabs(int index) {
    _currentindex = index;

    switch (index) {
      case 0:
        _patientMenuStatus = PatientMenuStatus.doctorScreen;
        break;

      case 1:
        _patientMenuStatus = PatientMenuStatus.recordScreen;
        break;
      case 2:
        _patientMenuStatus = PatientMenuStatus.pharmacyScreen;
        break;

      default:
        _patientMenuStatus = PatientMenuStatus.doctorScreen;
    }
    notifyListeners();
  }
}
