import 'package:cure_and_care_frontend/models/record_model.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PatientRecordsProvider with ChangeNotifier {
  bool isLoading = false;
  List<Record> _reports = [];

  List<Record> get getReports => _reports;

  Future<void> fetchPatientReports(BuildContext buildContext) async {
    _reports.clear();
    isLoading = true;
    notifyListeners();
    var contractProvider =
        Provider.of<ContractLinkingProvider>(buildContext, listen: false);

    var wallerProvider =
        Provider.of<WalletConnectProvider>(buildContext, listen: false);

    List<dynamic> list = await contractProvider
        .getPatientRecords(wallerProvider.getSessionStatus.accounts[0]);

    List<Record> newReports = [];

    int imageLen = list[0].length;
    int pdfLen = list[1].length;

    for (int i = 0; i < imageLen; i++) {
      Record record = Record.fromArray(list[0][i], false);
      newReports.add(record);
    }

    for (int i = 0; i < pdfLen; i++) {
      Record record = Record.fromArray(list[1][i], true);
      newReports.add(record);
    }

    newReports.sort((a, b) => b.dateOfCreation.compareTo(a.dateOfCreation));

    _reports = newReports;

    if (kDebugMode) {
      print("Fetched reports");
    }

    isLoading = false;
    notifyListeners();
  }

  Future<void> addPatientRecord(
      {required BuildContext buildContext,
      required BigInt doc,
      required String name,
      required String cId,
      required bool isPdf}) async {
    isLoading = true;
    notifyListeners();
    var contractProvider =
        Provider.of<ContractLinkingProvider>(buildContext, listen: false);

    var wallerProvider =
        Provider.of<WalletConnectProvider>(buildContext, listen: false);

    await contractProvider.addPatientRecord(
        name: name,
        cId: cId,
        // hex: "0xf24C1E0714558dceB95Ac4D833DB182055928cc0",
        hex: wallerProvider.getSessionStatus.accounts[0],
        doc: doc,
        isPdf: isPdf);

    if (kDebugMode) {
      print("Record added");
    }
    isLoading = false;
    notifyListeners();
  }
}
