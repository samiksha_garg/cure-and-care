import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';

class WalletConnectProvider with ChangeNotifier {
  //Managing metamask wallet connection status
  WalletConnectStatus _walletConnectStatus = WalletConnectStatus.uninitialized;

  late BuildContext _buildContext;
  bool _isUsingQRCode = false;

  late WalletConnect _connector;
  late SessionStatus
      _sessionStatus; //Containing details like address of the session connected
  late String uri; //Uri for deeplinking and QR code

  WalletConnectStatus get getWalletConnectStatus => _walletConnectStatus;
  SessionStatus get getSessionStatus => _sessionStatus;
  WalletConnect get getConnector => _connector;
  set setBuildContext(BuildContext buildContext) {
    _buildContext = buildContext;
  }

  set setIsUsingQRCode(bool isUsing) {
    _isUsingQRCode = isUsing;
  }

  WalletConnectProvider() {
    initialiseConection();
  }

  Future<void> initialiseConection() async {
    _connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
    );

    _connector.on('connect', (SessionStatus session) {
      _walletConnectStatus = WalletConnectStatus.connected;
      _sessionStatus = session;

      notifyListeners();
      if (_isUsingQRCode) {
        Navigator.pop(_buildContext);
      }
      if (kDebugMode) {
        print("connected");
      }
    });

    _connector.on('session_update', (SessionStatus payload) {
      _walletConnectStatus = WalletConnectStatus.updated;
      notifyListeners();
      if (kDebugMode) {
        print("session_update");
      }
    });

    _connector.on('disconnect', (session) {
      _walletConnectStatus = WalletConnectStatus.disconnected;
      notifyListeners();
      if (kDebugMode) {
        print("session_disconnect");
      }
    });

    if (!_connector.connected) {
      createSession();
    } else {
      _walletConnectStatus = WalletConnectStatus.connected;
      _sessionStatus = SessionStatus(
          chainId: _connector.session.chainId,
          accounts: _connector.session.accounts);
      notifyListeners();
    }
  }

  Future<void> createSession() async {
    await _connector.createSession(
      chainId: 4160,
      onDisplayUri: (url) {
        uri = url;
        _walletConnectStatus = WalletConnectStatus.initiated;
        notifyListeners();
      },
    );
  }
}
