import 'package:cure_and_care_frontend/models/patient_model.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:cure_and_care_frontend/providers/wallet_connect_provider.dart';
import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/doctor_model.dart';

class AuthServiceProvider with ChangeNotifier {
  late AuthenticationStatus _authStatus;
  late ContractLinkingProvider _contractLinkingProvider;
  late WalletConnectProvider _walletConnectProvider;
  bool isLoading = false;
  late DoctorModel _doctorModel;
  late PatientModel _patientModel;
  // String address = "0x266bCB712FAC535cD4BfC5612225EF9640cf0989";

  get getAuthStatus => _authStatus;
  DoctorModel get getDoctorModel => _doctorModel;
  PatientModel get getPatientModel => _patientModel;

  set setAuthStatus(AuthenticationStatus authenticationStatus) {
    _authStatus = authenticationStatus;
    notifyListeners();
  }

  set setWalletConnectProvider(WalletConnectProvider walletConnectProvider) {
    _walletConnectProvider = walletConnectProvider;
  }

  set setContractLinkingProvider(
      ContractLinkingProvider contractLinkingProvider) {
    _contractLinkingProvider = contractLinkingProvider;
  }

  Future<void> checkProfile({required String address}) async {
    var value = await _contractLinkingProvider.checkProfile(address);

    if (value == "Doctor") {
      _authStatus = AuthenticationStatus.registeredDoctor;
    } else if (value == "Patient") {
      _authStatus = AuthenticationStatus.registeredPatient;
    } else {
      _authStatus = AuthenticationStatus.unregistered;
    }

    notifyListeners();
  }

  Future<void> signUpPatient(
      {required String name,
      required String email,
      required BigInt dob,
      required String phone}) async {
    isLoading = true;
    notifyListeners();
    await _contractLinkingProvider.signUpPatient(
        hex: _walletConnectProvider.getSessionStatus.accounts[0],
        name: name,
        email: email,
        dob: dob,
        phoneNo: phone);
    isLoading = false;
    _authStatus = AuthenticationStatus.registeredPatient;
    notifyListeners();
  }

  Future<void> signUpDoctor(
      {required String name,
      required String email,
      required BigInt dob,
      required String phone,
      required String specialisation,
      required String regNo}) async {
    isLoading = true;
    notifyListeners();
    await _contractLinkingProvider.signUpDoctor(
        name: name,
        email: email,
        phoneNo: phone,
        dob: dob,
        specialisation: specialisation,
        regNo: regNo,
        hex: _walletConnectProvider.getSessionStatus.accounts[0]);

    isLoading = false;
    _authStatus = AuthenticationStatus.registeredDoctor;
    notifyListeners();
  }

  Future<void> getDoctorDetails(String hex) async {
    isLoading = true;
    notifyListeners();
    _doctorModel = await _contractLinkingProvider.getDoctorDetails(hex);
    isLoading = false;
    notifyListeners();
  }

  Future<void> getPatientDetails(String hex) async {
    isLoading = true;
    notifyListeners();
    _patientModel = await _contractLinkingProvider.getPatientDetails(hex);
    isLoading = false;
    notifyListeners();
  }
}
