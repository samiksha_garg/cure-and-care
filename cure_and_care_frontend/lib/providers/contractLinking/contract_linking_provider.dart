// ignore_for_file: unused_field, depend_on_referenced_packages

import 'dart:convert';
import 'dart:math';

import 'package:cure_and_care_frontend/models/doctor_model.dart';
import 'package:cure_and_care_frontend/models/patient_model.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/ethereum_transaction.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

class ContractLinkingProvider extends ChangeNotifier {
  late Web3Client _client;
  late EthereumWalletConnectProvider _provider;
  bool isLoading = false;
  late String _abiCode;
  late EthereumAddress _contractAddress;
  // late Credentials _credentials;
  late WalletConnectEthereumCredentials _credentials;
  late DeployedContract _contract;

  //Functions

  late ContractFunction _checkProfile;
  late ContractFunction _signUpPatient;
  late ContractFunction _signUpDoctor;
  late ContractFunction _getPatientDetails;
  late ContractFunction _getDoctorDetails;
  late ContractFunction _getPatientRecords;
  late ContractFunction _addPatientRecord;
  late ContractFunction _getAllDoctors;

  Future<void> initialSetUp(WalletConnect connector) async {
    isLoading = true;
    notifyListeners();

    var apiUrl = "http://127.0.0.1:7545";

    _client = Web3Client(
        // apiUrl,
        'https://goerli.infura.io/v3/70fe9cd181854940aa10494bbe5de9b5',
        Client());

    _provider = EthereumWalletConnectProvider(connector);
    await getAbi();
    await getCredentials();
    await getDeployedContract();
    isLoading = false;
    notifyListeners();
  }

  Future<void> getAbi() async {
    // Reading the contract abi
    String abiStringFile =
        await rootBundle.loadString("build_ethereum/HealthCare.json");
    var jsonAbi = jsonDecode(abiStringFile);
    _abiCode = jsonEncode(jsonAbi["abi"]);

    String addressAbiStringFile =
        await rootBundle.loadString("secrets/contract_address.json");
    var jsonAddress = jsonDecode(addressAbiStringFile);

    _contractAddress = EthereumAddress.fromHex(jsonAddress["contract_address"]);
  }

  Future<void> getCredentials() async {
    _credentials = WalletConnectEthereumCredentials(provider: _provider);
    // EthPrivateKey.fromHex(
    //     "8f7c62d19620e7098453928bdd3dd72a6c97c3a00b0d0d1e352911bb5c2a024e");
  }

  Future<void> getDeployedContract() async {
    // Telling Web3dart where our contract is declared.
    _contract = DeployedContract(
        ContractAbi.fromJson(_abiCode, "HelloWorld"), _contractAddress);

    if (kDebugMode) {
      print(_contract);
    }

    _checkProfile = _contract.function("checkProfile");
    _signUpPatient = _contract.function("signupPatient");
    _signUpDoctor = _contract.function("signupDoctor");
    _getDoctorDetails = _contract.function("getDoctorDetails");
    _getPatientDetails = _contract.function("getPatientDetails");
    _getPatientRecords = _contract.function("getPatientRecords");
    _addPatientRecord = _contract.function("addPatientsRecord");
    _getAllDoctors = _contract.function("getAllDoctors");
  }

  Future<dynamic> checkProfile(String hex) async {
    // var sender = await _credentials.extractAddress();
    final sender = EthereumAddress.fromHex(hex);
    var hash = await _client
        .call(contract: _contract, function: _checkProfile, params: [sender]);

    if (kDebugMode) {
      print(hash);
    }

    return hash[0];
  }

  Future<void> signUpPatient(
      {required String name,
      required String hex,
      required String email,
      required String phoneNo,
      required BigInt dob}) async {
    final sender = EthereumAddress.fromHex(hex);
    // var sender = await _credentials.extractAddress();

    var hash = await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            from: sender,
            contract: _contract,
            function: _signUpPatient,
            parameters: [name, dob, email, phoneNo]));

    if (kDebugMode) {
      print(hash);
    }

    while (true) {
      var transactionInfo = await _client.getTransactionReceipt(hash);
      if (transactionInfo == null) {
        continue;
      } else {
        break;
      }
    }
  }

  Future<void> signUpDoctor(
      {required String name,
      required String email,
      required String phoneNo,
      required BigInt dob,
      required String specialisation,
      required String regNo,
      required String hex}) async {
    final sender = EthereumAddress.fromHex(hex);
    if (kDebugMode) {
      print("Signing Doctor");
    }
    // var sender = await _credentials.extractAddress();
    var hash = await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            from: sender,
            contract: _contract,
            function: _signUpDoctor,
            parameters: [name, dob, email, phoneNo, specialisation, regNo]));

    if (kDebugMode) {
      print(hash);
    }

    while (true) {
      var transactionInfo = await _client.getTransactionReceipt(hash);
      if (transactionInfo == null) {
        continue;
      } else {
        break;
      }
    }
  }

  Future<PatientModel> getPatientDetails(String hex) async {
    final sender = EthereumAddress.fromHex(hex);

    var hash = await _client.call(
        sender: sender,
        contract: _contract,
        function: _getPatientDetails,
        params: []);

    if (kDebugMode) {
      print(hash);
    }

    return PatientModel(
        name: hash[0],
        email: hash[2],
        dateOfBirth: DateTime.fromMicrosecondsSinceEpoch(hash[1].toInt()),
        phoneNo: hash[3]);
  }

  Future<DoctorModel> getDoctorDetails(String hex) async {
    final sender = EthereumAddress.fromHex(hex);

    var hash = await _client.call(
        sender: sender,
        contract: _contract,
        function: _getDoctorDetails,
        params: []);

    if (kDebugMode) {
      print(hash);
    }

    return DoctorModel(
        name: hash[0],
        email: hash[2],
        dateOfBirth: DateTime.fromMicrosecondsSinceEpoch(hash[1].toInt()),
        phoneNo: hash[3],
        specialisation: hash[4],
        registerationNumber: hash[5],
        ethAddress: hex);
  }

  Future<List<dynamic>> getPatientRecords(String hex) async {
    final sender = EthereumAddress.fromHex(hex);
    var hash = await _client.call(
        sender: sender,
        contract: _contract,
        function: _getPatientRecords,
        params: [sender]);

    if (kDebugMode) {
      print(hash);
    }

    return hash;
  }

  Future<void> addPatientRecord(
      {required String name,
      required String cId,
      required String hex,
      required BigInt doc,
      required bool isPdf}) async {
    final sender = EthereumAddress.fromHex(hex);
    var hash = await _client.sendTransaction(
        _credentials,
        Transaction.callContract(
            from: sender,
            contract: _contract,
            function: _addPatientRecord,
            parameters: [name, cId, doc, isPdf]));

    if (kDebugMode) {
      print(hash);
    }

    while (true) {
      var transactionInfo = await _client.getTransactionReceipt(hash);
      if (transactionInfo == null) {
        continue;
      } else {
        break;
      }
    }
  }

  Future<dynamic> getAllDoctors() async {
    var hash = await _client
        .call(contract: _contract, function: _getAllDoctors, params: []);

    if (kDebugMode) {
      print(hash);
    }

    return hash;
  }
}
