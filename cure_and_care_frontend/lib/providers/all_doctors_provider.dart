import 'package:cure_and_care_frontend/models/doctor_model.dart';
import 'package:cure_and_care_frontend/providers/contractLinking/contract_linking_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AllDoctorsProvider with ChangeNotifier {
  bool isLoading = false;
  List<DoctorModel> _allDoctors = [];

  List<DoctorModel> get getAllDoctors => _allDoctors;

  Future<void> fetchAllDoctors(BuildContext buildContext) async {
    isLoading = true;
    notifyListeners();

    _allDoctors.clear();

    List<DoctorModel> allDoctors = [];

    var contractLinkingProvider =
        Provider.of<ContractLinkingProvider>(buildContext, listen: false);
    var doctors = await contractLinkingProvider.getAllDoctors();
    doctors = doctors[0];

    for (int i = 0; i < doctors.length; i++) {
      allDoctors.add(DoctorModel.fromArray(doctors[i]));
    }

    _allDoctors = allDoctors;

    isLoading = false;
    notifyListeners();
  }
}
