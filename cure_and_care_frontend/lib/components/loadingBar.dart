import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class LoadingBarWidget extends StatelessWidget {
  const LoadingBarWidget(
      {Key? key, required this.child, required this.isLoading})
      : super(key: key);
  final Widget child;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
        progressIndicator: Container(
            padding: EdgeInsets.all(20.r),
            decoration: BoxDecoration(
                color: HexColor(HexColors.pastelPink), shape: BoxShape.circle),
            child: LoadingAnimationWidget.threeRotatingDots(
                color: HexColor(HexColors.seaPink), size: 90.w)),
        inAsyncCall: isLoading,
        child: child);
  }
}
