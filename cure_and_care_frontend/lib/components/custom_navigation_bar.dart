import 'package:cure_and_care_frontend/providers/bottom_navigation_bar.dart';
import 'package:cure_and_care_frontend/utils/colors.dart';
import 'package:cure_and_care_frontend/utils/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class CustomNavigationBar extends StatelessWidget {
  const CustomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<BottomNavigationBarProvider>(
        builder: (context, bottomNav, child) {
      return Container(
        // padding: EdgeInsets.symmetric(vertical: 1.h),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            ),
          ],
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40.r),
            topRight: Radius.circular(40.r),
          ),
        ),
        child: SafeArea(
            top: false,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.home,
                    size: 30.sp,
                    color:
                        PatientMenuStatus.doctorScreen == bottomNav.currentPage
                            ? HexColor(HexColors.denimBlue)
                            : HexColor(HexColors.inactiveColor),
                  ),
                  onPressed: () => bottomNav.toggleTabs(0),
                ),
                IconButton(
                  icon: Icon(
                    Icons.file_present,
                    size: 30.sp,
                    color:
                        PatientMenuStatus.recordScreen == bottomNav.currentPage
                            ? HexColor(HexColors.denimBlue)
                            : HexColor(HexColors.inactiveColor),
                  ),
                  onPressed: () => bottomNav.toggleTabs(1),
                ),
                IconButton(
                  icon: Icon(
                    Icons.medical_information,
                    size: 30.sp,
                    color: PatientMenuStatus.pharmacyScreen ==
                            bottomNav.currentPage
                        ? HexColor(HexColors.denimBlue)
                        : HexColor(HexColors.inactiveColor),
                  ),
                  onPressed: () => bottomNav.toggleTabs(2),
                ),
              ],
            )),
      );
    });
  }
}
