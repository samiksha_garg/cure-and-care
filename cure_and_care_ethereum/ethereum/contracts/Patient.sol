// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Patient {
     struct patient {
        string name;
        address id;
        uint dob;
        string email;
        string phoneNo;
    }

    struct recordDetails {
        string recordHash;
        string recordName;
        uint dateOfCreation;
    }

     struct record{
        recordDetails[] pdfRecordHashes;
        recordDetails[] imageRecordHashes;
        address patientId;
        mapping (address => bool) accessDoctors;
    }

    mapping (address => record) internal records;

    mapping (address => patient) internal patients;

    function signupPatient(string memory _name, uint _dob, string memory _email, string memory _phone) public {
        patient storage p = patients[msg.sender];
        record storage r = records[msg.sender];


        require(!(p.id > address(0x0)));
        
        r.patientId = msg.sender;
        

        patients[msg.sender] = patient({name:_name,id:msg.sender, dob:_dob, email:_email, phoneNo:_phone});
        
    }

    function getPatientDetails() public view returns(string memory, uint, string memory, string memory){
         patient storage p = patients[msg.sender];
         return (p.name, p.dob, p.email, p.phoneNo);
    }

    function addPatientsRecord(string memory _name, string memory _hash, uint doc, bool isPdf) public {
        
        patient storage p = patients[msg.sender];
        require(p.id > address(0x0));

        record storage r = records[msg.sender];

        if (!isPdf) {
             recordDetails[] storage recD = r.imageRecordHashes;
             recD.push(recordDetails({recordHash : _hash, recordName : _name, dateOfCreation : doc}));
        } else {
            recordDetails[] storage recD = r.pdfRecordHashes;
             recD.push(recordDetails({recordHash : _hash, recordName : _name, dateOfCreation : doc}));
        }

    }

   
}