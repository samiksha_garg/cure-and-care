// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

contract Doctor {
    struct doctor {
        string name;
        address id;
        string email;
        uint dob;
        string specialisation;
        string registerationNumber;
        string phoneNo;
    }

    mapping (address => doctor) internal doctors;
    doctor[] internal allDoctors;

    function signupDoctor(string memory _name, uint _dob, string memory _email, string memory _phone, string memory _specialisation, string memory _registerationNo) public {
        doctor storage d = doctors[msg.sender];
        
        require(!(d.id > address(0x0)));
        
        doctors[msg.sender] = doctor({name:_name,id:msg.sender, dob:_dob, email:_email, phoneNo:_phone, specialisation:_specialisation, registerationNumber : _registerationNo});
        allDoctors.push(doctors[msg.sender]);
    }

    function getDoctorDetails() public view returns(string memory, uint, string memory, string memory, string memory, string memory){
         doctor storage d = doctors[msg.sender];
         return (d.name, d.dob, d.email, d.phoneNo, d.specialisation, d.registerationNumber);
    }

    function getAllDoctors() public view returns(doctor[] memory) {
        return allDoctors;
    }
}