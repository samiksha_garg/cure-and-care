// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./Doctor.sol";
import "./Patient.sol";

contract HealthCare is Doctor, Patient {


    function checkProfile(address _user) public view returns(string memory){
        patient storage p = patients[_user];
        doctor storage d = doctors[_user];
          
        if(p.id > address(0x0))
            return ('Patient');
        else if(d.id > address(0x0))
            return ('Doctor');
        else
            return ('');
    }

    function getPatientRecords(address _user) public view returns(recordDetails[] memory, recordDetails[] memory, address) {
        record storage rec = records[_user];
        require(msg.sender == _user || rec.accessDoctors[_user] == true);
        return (rec.imageRecordHashes, rec.pdfRecordHashes, rec.patientId);
    }

    function giveRecordAccessToDoctor(address _doctor) public {
        
        doctor storage d = doctors[_doctor];
        patient storage p = patients[msg.sender];
        require(d.id > address(0x0));
        require(p.id > address(0x0));

        record storage rec = records[msg.sender];
        rec.accessDoctors[_doctor] = true;
    }
}