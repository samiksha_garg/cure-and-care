const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require("web3");
const compiledFactory = require("../../cure_and_care_frontend/build_ethereum/HealthCare.json");
const dotenv = require("dotenv");
const fs = require("fs-extra");
const path = require("path");

dotenv.config();

const provider = new HDWalletProvider(
  process.env.PRIVATE_KEY,
  process.env.INFURA_ENDPOINT
);
const web3 = new Web3(provider);

const addressPath = "../../cure_and_care_frontend/secrets";
fs.ensureDirSync(addressPath);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log("Attempting to deploy from account", accounts[0]);

  const result = await new web3.eth.Contract(compiledFactory["abi"])
    .deploy({ data: compiledFactory["evm"]["bytecode"]["object"] })
    .send({
      gas: 2500000,
      from: accounts[0],
      // gasPrice: web3.utils.toHex(web3.utils.toWei("30", "gwei")),
    });

  const contractAddress = {
    contract_address: result.options.address,
  };

  fs.outputJsonSync(
    path.resolve(addressPath, "contract_address.json"),
    contractAddress
  );

  console.log("Contract deployed to", result.options.address);
  provider.engine.stop();
};
deploy();
